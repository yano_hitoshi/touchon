#pragma once
#include "StageCreatorBase.h"
#include "RapidJsonHelper.h"
#include <vector>

/*
@file FirstStageCreator.h
@brief FirstStageを生成する
*/
class FirstStageCreator 
	 : public StageCreatorBase
{
public:
	FirstStageCreator(bool _reUseGameObject, const Tag _objectTag);
	~FirstStageCreator()override;
	/*
	@fn ファイルを開く
	@return 成功か否か
	*/
	bool OpenFile();

	/*
	@fn プレイヤーを生成する
	*/
	class PlayerObject* CreatePlayer();

	/*
	@fn ステージを生成する
	*/
	void CreateStage();

private:

	//ファーストステージデータ
	std::vector<std::vector<int>> layer1StageData;
	std::vector<std::vector<int>> layer2StageData;
	std::vector<std::vector<int>> layer3StageData;
	std::vector<std::vector<int>> layer4StageData;
	std::vector<std::vector<int>> layer5StageData;

	//playerのデータ
	std::vector<std::vector<int>> playerData;

	//配置するオブジェクトの間隔X
	int sizeX;
	//配置するオブジェクトの間隔Y
	int sizeY;
	//配置するオブジェクトの間隔。サイズ
	float offset;

};

